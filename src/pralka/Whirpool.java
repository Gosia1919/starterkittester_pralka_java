package pralka;

public class Whirpool extends Pralka {
	
	public Whirpool(int actuallProgram, double temp, int speedV) {
		super(actuallProgram, temp, speedV, 25);
		super.setProducent(Producent.WHIRPOOL);
	}
}
