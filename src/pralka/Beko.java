package pralka;

public class Beko extends Pralka {

	public Beko(int actuallProgram, double temp, int speedV) {
		super(actuallProgram, temp, speedV);
		super.setProducent(Producent.BEKO); 
		this.stepOfTemp = 1.0f;
	}
	
	@Override
	public void setTemp(double temp) {
		if(temp < 0 || temp > 90) {
			System.out.println("Temperature has to be in range 0-90 degrees");
			//throw new RuntimeException();
		} else {
			this.temp = (Math.round(temp)); 					
		}
	}
}
