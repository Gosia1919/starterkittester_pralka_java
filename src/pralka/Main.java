package pralka;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Main {
	public static void main(String[] args) {
		
		System.out.println("**********Amica*************");
		
		Amica amica1 =null;
		try {
			amica1 = new Amica(19, 78.2, 810);
		} catch(ProgramNumberOutRangeException | TemperatureOutRangeException | SpeedRotationOutRangeException e ) {
			e.printStackTrace();
		}
			amica1.showStatus();
			amica1.upTemp();
			amica1.showStatus();
			amica1.downTemp();
			amica1.showStatus();
			amica1.nextProgram();
			amica1.showStatus();
			amica1.upSpeedV();
			amica1.showStatus();
			
			System.out.println();
			System.out.println("**********Beko*************");
			Beko beko1 = new Beko(19, 30.6, 561);
			beko1.showStatus();
			beko1.upTemp();
			beko1.showStatus();
			beko1.downTemp();
			beko1.showStatus();
			beko1.nextProgram();
			beko1.showStatus();
			beko1.upSpeedV();
			beko1.showStatus();
			
			System.out.println();
			System.out.println("**********Whirpool*************");
			Whirpool whirpool1 = new Whirpool(24, 30.6, 561);
			whirpool1.showStatus();
			whirpool1.upTemp();
			whirpool1.showStatus();
			whirpool1.downTemp();
			whirpool1.showStatus();
			whirpool1.nextProgram();
			whirpool1.showStatus();
			whirpool1.upSpeedV();
			whirpool1.showStatus();
		 
		List<Pralka> pralkaList = new ArrayList<>();
		pralkaList.add(beko1);
		pralkaList.add(whirpool1);
		pralkaList.add(amica1);
		
		System.out.println();
		System.out.println("*************Wypisz liste****************");
		writeListAlpfabetOrder(pralkaList);
	}
	
	public static void writeListAlpfabetOrder(List<Pralka> pralkaList) {
		System.out.println("***********Before sort****************");
		pralkaList.stream().forEach(System.out::println);
		System.out.println("***********After sort*****************");
		pralkaList.stream()
			.sorted((a ,b) -> a.getProducent().name().compareTo(b.getProducent().name()))
			.collect(Collectors.toList())
			.forEach(System.out::println);		
	}
}
