package pralka;

public class SpeedRotationOutRangeException extends RuntimeException{
	static String messageInAll = "Speed rotation is out range. The range is: 0-1000 rotate/min ";
	
	public SpeedRotationOutRangeException() {
		super(messageInAll);
	}
}
