package pralka;

public class ProgramNumberOutRangeException extends RuntimeException {
	static String messageInAll = "The number of programs is out range. The range is: 1-";
	
	public ProgramNumberOutRangeException(int maxPrograms) {
		super(messageInAll + maxPrograms);
	}
}
