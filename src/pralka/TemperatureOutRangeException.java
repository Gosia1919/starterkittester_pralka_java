package pralka;

public class TemperatureOutRangeException extends RuntimeException{
	static String messageInAll = "Temperature is out range. The range is: 0-90 " + (char)176 + "C";
	
	public TemperatureOutRangeException() {
		super(messageInAll);
	}
}
