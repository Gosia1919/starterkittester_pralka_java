package pralka;

public class Amica extends Pralka {

	public Amica(int actuallProgram, double temp, int speedV) {
		super(actuallProgram, temp, speedV);
		super.setProducent(Producent.AMICA); 
	}
}
