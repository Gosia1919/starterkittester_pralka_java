package pralka;

public abstract class Pralka {
	private int actuallProgram;
	protected double temp;
	private int speedV;
	private Producent producent;
	protected int maxNumberOfPrograms = 20;
	protected float stepOfTemp = 0.5f;
	
	public Pralka() {
		
	}
	
	public Pralka(int actuallProgram, double temp, int speedV) {
		setActuallProgram(actuallProgram);
		setTemp(temp);			
		setSpeedV(speedV);
	}
	
	public Pralka(int actuallProgram, double temp, int speedV, int maxNumberOfPrograms) {
		this.maxNumberOfPrograms = maxNumberOfPrograms;
		setActuallProgram(actuallProgram);
		setTemp(temp);			
		setSpeedV(speedV);			
	}
	
	public void nextProgram() {
		if(this.actuallProgram == maxNumberOfPrograms) {
			this.actuallProgram = 1;
		} else {
			this.actuallProgram++;			
		}
	}
	
	public void previousProgram() {
		if(this.actuallProgram == 1) { 
			this.actuallProgram = maxNumberOfPrograms;
		} else {
			this.actuallProgram --;			
		}
	}
	
	public void upTemp() {
		if(this.temp >= 90) {
			System.out.println("The temp is max. You can't up the temperature");
		} else {
			this.temp += stepOfTemp;
			System.out.println("Current temperature is: " + this.temp +(char)176 + "C");			
		}
	}
	
	public void downTemp() {
		if(this.temp <= 0) {
			System.out.println("The temp is min. You can't down the temperature");
		} else {
			this.temp -= stepOfTemp;
			System.out.println("Current temperature is: " + this.temp +(char)176 + "C");			
		}
	}
	
	public void upSpeedV() {
		if(this.speedV == 1000) {
			this.speedV = 0;
		} else {
			this.speedV += 100;			
		}
	}
	
	public void downSpeedV() {
		if(this.speedV == 0) {
			this.speedV= 1000;
		} else {
			this.speedV -= 100;			
		}
	}
	
	public void showStatus() {
		System.out.println("Actuall program: " + this.actuallProgram + ", Temperature: " + this.temp + 
				", speed V: " + this.speedV);
	}
	
	public void setActuallProgram(int actuallProgram) {
		if(actuallProgram < 1 || actuallProgram > maxNumberOfPrograms)  {
			throw new ProgramNumberOutRangeException(maxNumberOfPrograms);				
		} else {
			this.actuallProgram = actuallProgram;			
		}
	}
	
	public int getActuallProgram() {
		return actuallProgram;
	}
	
	public void setTemp(double temp) {
		if(temp < 0 || temp > 90) {
			throw new TemperatureOutRangeException(); 				
		} else {
			this.temp = Math.round(temp * 2) / 2.0;					
		}
	}
	
	public double getTemp() {
		return temp;
	}
	
	public void setSpeedV(int speedV) {
		if(speedV < 0 || speedV > 1000) {
			throw new SpeedRotationOutRangeException();				
		} else {
			this.speedV = (int)(Math.round(speedV / 100.0) * 100);					
		}
	}
	
	public int getSpeedV() {
		return speedV;
	}
	
	public Producent getProducent() {
		return producent;
	}
	
	public void setProducent(Producent producent) {
		this.producent = producent;
	}

	public int getMaxNumberOfPrograms() {
		return maxNumberOfPrograms;
	}

	public void setMaxNumberOfPrograms(int maxNumberOfProgramms) {
		this.maxNumberOfPrograms = maxNumberOfProgramms;
	}

	@Override
	public String toString() {
		return producent + " " + "{actuallProgram: " + actuallProgram + ", temp: " + temp
		+ ", speedV: " + speedV + ", maxNumberOfPrograms: " + maxNumberOfPrograms + "}";
	}
}
